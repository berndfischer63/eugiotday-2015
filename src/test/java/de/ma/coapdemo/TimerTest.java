package de.ma.coapdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import javax.inject.Inject;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.CoapServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration( classes = App.class )
@DirtiesContext( classMode = ClassMode.AFTER_EACH_TEST_METHOD )
public class TimerTest
{
	@Inject Integer    coapPort;
	@Inject CoapServer coapServer;

	Logger log = LoggerFactory.getLogger( this.getClass() );

	@Test public void testSomething()
	{
		assertThat( this.coapPort  , is( not( 0 ) ) );
		assertThat( this.coapServer, is( notNullValue() ) );
	}

	boolean touched = false;

	@Test public void observableTest() throws Exception
	{
		CoapClient coapClient = this.createCoapClient();
		CoapObserveRelation rel = coapClient.observe( new CoapHandler(){
			@Override
			public void onLoad( CoapResponse response )
			{
				TimerTest.this.touched = true;
				TimerTest.this.log.debug( "onLoad: {}", response.getResponseText() );
			}
			@Override
			public void onError()
			{
				TimerTest.this.log.debug( "onError()" );
			}
		});

		Thread.sleep( 2000 );

		CoapResponse rsp = rel.getCurrent();
		assertThat( rsp, is( notNullValue() ) ) ;
		assertThat( rsp.isSuccess(), is( true ) );

		assertThat( this.touched, is ( true ) );

		rel.proactiveCancel();
	}

	// --------------------------------------------------------------------------------------------------------

	private CoapClient createCoapClient()
	{
		CoapClient coapClient = this.createCoapClient(
			"coap://localhost:" + this.coapPort + "/" + TimeResource.URL_PART
		);
		return coapClient;
	}

	private CoapClient createCoapClient( String url )
	{
		CoapClient coapClient = new CoapClient( url	);
		coapClient.useNONs();
		return coapClient;
	}
}
