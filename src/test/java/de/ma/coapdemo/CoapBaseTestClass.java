package de.ma.coapdemo;

import org.eclipse.californium.core.CoapServer;
import org.junit.After;
import org.junit.Before;

import de.ma.cmn.BaseTestClass;

public class CoapBaseTestClass extends BaseTestClass
{
	private CoapServer server;

	@Override
	@Before
	public void before() throws Exception
	{
		super.before();

		this.server = new CoapServer( 25683 );
		this.server.add( new HelloWorldResource() );
		this.server.start();
	}

	@Override
	@After
	public void after() throws Exception
	{
		if( this.server != null )
		{
			this.server.stop();
			this.server.destroy();
		}

		super.after();
	}
}
