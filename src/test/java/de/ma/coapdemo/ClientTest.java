package de.ma.coapdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.junit.Test;

public class ClientTest extends CoapBaseTestClass
{
	@Test
	public void testSimpleLocalGetRequest()
	{
		CoapClient   client = new CoapClient( "coap://localhost:25683/Hello" );
		client.useNONs();
		CoapResponse rsp = client.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "Hello Eclipse World" ) );

		rsp = client.put( "Hello Eclipse World@Dresden", MediaTypeRegistry.TEXT_PLAIN );

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );

		rsp = client.get();
		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "Hello Eclipse World@Dresden" ) );
	}
}
