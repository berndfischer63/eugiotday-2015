package de.ma.coapdemo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Set;

import javax.inject.Inject;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration( classes = App.class )
@DirtiesContext( classMode = ClassMode.AFTER_EACH_TEST_METHOD )
public class WorkshopTest
{
	@Inject Integer    coapPort;
	@Inject CoapServer coapServer;

	Logger log = LoggerFactory.getLogger( this.getClass() );

	@Test public void testSomething()
	{
		assertThat( this.coapPort  , is( not( 0 ) ) );
		assertThat( this.coapServer, is( notNullValue() ) );

	}

	@Test public void testPing()
	{
		CoapClient coapClient = createCoapClient();
		assertThat( coapClient.ping(), is( true ) );
	}

	@Test public void if_NotAnyWorkshopsExist_Get_ShouldReturnEmptyString()
	{
		CoapClient coapClient = createCoapClient();

		CoapResponse rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "" ) );
	}

	@Test public void if_OneWorkshopExist_Get_ShouldReturnListWithOneEntry()
	{
		CoapClient coapClient = this.createCoapClient();

		CoapResponse rsp = coapClient.post( "Eclipse IoT Day", MediaTypeRegistry.TEXT_PLAIN );

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getCode()  , is( ResponseCode.CREATED ) );

		rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "Eclipse IoT Day" ) );
	}

	@Test public void if_WorkShopExists_Delete_shouldRemoveIt()
	{
		String workshopName = "Eclipse-IoT-Day";

		CoapClient coapClient = this.createCoapClient();

		CoapResponse rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "" ) );

		rsp = coapClient.post( workshopName, MediaTypeRegistry.TEXT_PLAIN );

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getCode()  , is( ResponseCode.CREATED ) );

		rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( workshopName ) );

		String uri = coapClient.getURI();
		CoapClient coapClient2 = this.createCoapClient( uri + "/" + workshopName );

		rsp = coapClient2.delete();
		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );

		rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "" ) );
	}

	@Test public void if_WorkShopExists_Discovery_shouldListIt()
	{
		String workshopName = "Eclipse-IoT-Day";

		CoapClient coapClient = this.createCoapClient();

		CoapResponse rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( "" ) );

		rsp = coapClient.post( workshopName, MediaTypeRegistry.TEXT_PLAIN );

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getCode()  , is( ResponseCode.CREATED ) );

		rsp = coapClient.get();

		assertThat( rsp, is( notNullValue() ) );
		assertThat( rsp.isSuccess(), is( true ) );
		assertThat( rsp.getResponseText(), is( workshopName ) );

		Set<WebLink> links = coapClient.discover();

		assertThat( links       , is( notNullValue() ) );
		assertThat( links.size(), is( 4 ) );
		assertThat( links.contains( new WebLink( "/.well-known/core"          ) ), is( true ) );
		assertThat( links.contains( new WebLink( "/workshops"                 ) ), is( true ) );
		assertThat( links.contains( new WebLink( "/workshops/Eclipse-IoT-Day" ) ), is( true ) );

		this.log.debug( "{} links found", links.size() );

		for( WebLink link : links )
		{
			this.log.debug( "link: {}", link.toString() );
		}

		links = coapClient.discover( "/" );

		this.log.debug( "{} links found", links.size() );

		for( WebLink link : links )
		{
			this.log.debug( "link: {}", link.toString() );
		}
	}

	// --------------------------------------------------------------------------------------------------------

	private CoapClient createCoapClient()
	{
		CoapClient coapClient = this.createCoapClient(
			"coap://localhost:" + this.coapPort + "/" + WorkshopsResource.URL_PART
		);
		return coapClient;
	}

	private CoapClient createCoapClient( String url )
	{
		CoapClient coapClient = new CoapClient( url	);
		coapClient.useNONs();
		return coapClient;
	}
}
