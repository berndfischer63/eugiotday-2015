package de.ma.coapdemo;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.config.NetworkConfig;

public class Server
{
	public static void main( String[] args )
	{
		NetworkConfig config = NetworkConfig.getStandard();
		int           ports  = 5683;
		CoapServer    server = new CoapServer( config, ports );

		server.add( new HelloWorldResource() );

		server.start();
	}
}
