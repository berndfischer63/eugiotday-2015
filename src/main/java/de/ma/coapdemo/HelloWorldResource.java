package de.ma.coapdemo;

import java.io.UnsupportedEncodingException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class HelloWorldResource extends CoapResource
{
	private String value = "Hello Eclipse World";

	public HelloWorldResource()
	{
		super( "Hello" );
		this.getAttributes().setTitle( "Hello World Resource" );
	}

	@Override
	public void handleGET( CoapExchange exchange )
	{
		exchange.respond( this.value );
	}

	@Override
	public void handlePOST( CoapExchange exchange )
	{
		super.handlePOST( exchange );
	}

	@Override
	public void handlePUT( CoapExchange exchange )
	{
		String tmp = null;

		try
		{
			tmp = new String( exchange.getRequestPayload(), "UTF-8" );
		}
		catch( UnsupportedEncodingException e )
		{
			// nothing real to do because UTF-8 is supported
			throw new RuntimeException( e );
		}

		// tmp can't be null at this place
		this.value = tmp;

		exchange.respond( ResponseCode.CHANGED, this.value );
	}

	@Override
	public void handleDELETE( CoapExchange exchange )
	{
		super.handleDELETE( exchange );
	}
}
