package de.ma.coapdemo;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class App
{
	@Value( "${coapServer.port}" ) int port  = 5683;

	@Bean
	public Integer getCoapPort()
	{
		return new Integer( this.port );
	}

	@Bean( initMethod="start", destroyMethod="stop" )
	public CoapServer coapServer()
	{
		NetworkConfig config = NetworkConfig.getStandard();
		CoapServer    server = new CoapServer( config, this.getCoapPort() );

		server.add( new WorkshopsResource() );
		server.add( new TimeResource() );

		return server;
	}

	// --------------------------------------------------------------------------------------------------------

	public static void main( String[] args )
	{
		SpringApplication.run( App.class, args );
	}
}
