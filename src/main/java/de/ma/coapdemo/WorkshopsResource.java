package de.ma.coapdemo;

import java.util.Collection;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.Resource;

public class WorkshopsResource extends CoapResource
{
	public static final String URL_PART = "workshops";

	public WorkshopsResource()
	{
		super( URL_PART );
		this.getAttributes().setTitle( "All Workshops" );
	}

	@Override
	public void handleGET( CoapExchange exchange )
	{
		StringBuffer sb = new StringBuffer();

		Collection<Resource> children = this.getChildren();
		for( Resource child : children )
		{
			sb.append( child.getName() + "\n" );
		}

		exchange.respond( sb.toString() );
	}

	@Override
	public void handlePOST( CoapExchange exchange )
	{
		String payload = exchange.getRequestText();
		if( payload == null || payload.length() == 0 )
		{
			exchange.respond( ResponseCode.BAD_REQUEST );
		}
		else
		{
			this.add( new WorkshopResource( payload ) );
			exchange.respond( ResponseCode.CREATED );
		}
	}
}
