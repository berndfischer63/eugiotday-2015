package de.ma.coapdemo;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class WorkshopResource extends CoapResource
{
	public WorkshopResource( String urlPart )
	{
		super( urlPart );
		this.getAttributes().setTitle( "Workshop " + urlPart );
	}

	@Override
	public void handleDELETE( CoapExchange exchange )
	{
		this.delete();
		exchange.respond( ResponseCode.DELETED );
	}
}
