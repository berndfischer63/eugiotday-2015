package de.ma.coapdemo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.californium.core.CoapResource;
//import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class TimeResource extends CoapResource
{
	public static final String URL_PART = "currentTime";

	public TimeResource()
	{
		super( URL_PART );
		this.getAttributes().setTitle( "Current Time" );

		this.setObservable( true );
		this.setObserveType( Type.NON );
		this.getAttributes().setObservable();

		Timer timer = new Timer();
		timer.schedule( new UpdateTask(), 0, 1000 );
	}

	private SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );

	@Override
	public void handleGET( CoapExchange exchange )
	{
		exchange.setMaxAge( 1 );
		exchange.respond( "now: " + this.sdf.format( Calendar.getInstance().getTime() ) );
	}

	@Override
	public void handlePUT( CoapExchange exchange )
	{
		super.handlePUT( exchange );
	}

	@Override
	public void handleDELETE( CoapExchange exchange )
	{
		super.handleDELETE( exchange );
//		 delete(); // will also call clearAndNotifyObserveRelations(ResponseCode.NOT_FOUND)
//		 exchange.respond( ResponseCode.DELETED );
	}

	// --------------------------------------------------------------------------------------------------------

	private class UpdateTask extends TimerTask
	{
		public UpdateTask()
		{
			// nothing to do
		}

		@Override
		public void run()
		{
			TimeResource.this.changed();
		}
	}
}
